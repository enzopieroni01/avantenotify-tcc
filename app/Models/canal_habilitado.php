<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Canal_habilitado extends Model
{
    use HasFactory;

    public function canal(): BelongsTo
    {
        return $this->belongsTo(Canal::class);
    }
    protected $fillable = [
       'usaurio_id',
       'canal_id'
    ];
}
