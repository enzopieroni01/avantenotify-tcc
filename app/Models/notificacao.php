<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Notificacao extends Model
{
    use HasFactory;
    protected $table = 'notificacao';
    public $timestamps = false;

    public function canal_notificacaos(): HasMany
    {
        return $this->hasMany(Canal_notificacao::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function remetente(): BelongsTo
    {
        return $this->belongsTo(User::class, 'usuario_remetente_id');
    }

    public function destinatario(): BelongsTo
    {
        return $this->belongsTo(User::class, 'usuario_destinatario_id');
    }

    protected $fillable = [
        'mensagem',
        'usuario_remetente_id',
        'usuario_destinatario_id',
        'dataHora',
        'lido'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];
}
