<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Canal_notificacao extends Model
{
    use HasFactory;

    public function canal(): BelongsTo
    {
        return $this->belongsTo(Canal::class);
    }

    protected $fillable = [
        'notificacao_id',
        'usuarioDestinatario_id',
        'dataHora'
    ];
}
