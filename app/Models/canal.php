<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Canal extends Model
{
    use HasFactory;

    public function canal_habilitados(): HasMany
    {
        return $this->hasMany(Canal_habilitado::class);
    }

    public function canal_notificacaos(): HasMany
    {
        return $this->hasMany(canal_notificacao::class);
    }

    protected $fillable = [
        'nome'
    ];
}
