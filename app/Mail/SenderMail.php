<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class SenderMail extends Mailable
{
    use Queueable, SerializesModels;

    public $conteudoEmail;
    public $emailDestinatario;

    /**
     * Create a new message instance.
     */
    public function __construct($conteudoEmail,$emailDestinatario)
    {
        $this->conteudoEmail = $conteudoEmail;
        $this->emailDestinatario = $emailDestinatario;
    }

    /**
     * Get the message envelope.
     */
    public function envelope(): Envelope
    {
        return new Envelope(
            subject: 'Avante Notify Mail',
        );
    }

    /**
     * Get the message content definition.
     */
    public function content(): Content
    {
        return new Content(
            view: 'emailTemplates.testeEmail',
            with: [
                'conteudoEmail' => $this->conteudoEmail, 
                'emailDestinatario' => $this->emailDestinatario, 
            ]
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array<int, \Illuminate\Mail\Mailables\Attachment>
     */
    public function attachments(): array
    {
        return [];
    }
}
