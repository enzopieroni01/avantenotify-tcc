<?php

namespace App\Http\Controllers;

use App\Mail\SenderMail;

use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class MailController extends Controller
{
    public function main()
    {
        return view('emails.emailSender');
    }

    public function index(Request $request)
    {
        try {
            $emailDestinatario = $request->input('toEmail');
            $conteudoEmail = $request->input('conteudoEmail');
            Mail::to($emailDestinatario)->send(new SenderMail($conteudoEmail, $emailDestinatario));
            $msgSucess = 'Email enviado com sucesso!';
            return response()->json(['message' => $msgSucess], 200); 
        } catch (Exception $e) {
            Log::error("Erro ao enviar email: " . $e->getMessage());
            return response()->json(['message' => 'Erro ao enviar email'], 500);
        }
    }

    public function listEmails()
    {
        $client = new Client();

        $url = 'https://mailtrap.io/api/accounts/1920898/inboxes/2866593/messages'; 

        $response = $client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Api-Token' => 'c19a356c15cb389cfe416ded9e52d01f',
            ],
        ]);

        $emails = json_decode($response->getBody()->getContents(), true);

        return view('emails.inboxEmails', compact('emails'));
    }

    public function showEmail($inboxId, $messageId)
    {
        $accountId = '1920898';

        $client = new Client();

        try {
            $response = $client->request('GET', "https://mailtrap.io/api/accounts/{$accountId}/inboxes/{$inboxId}/messages/{$messageId}/body.html", [
                'headers' => [
                    'Accept' => 'application/json',
                    'Api-Token' => 'c19a356c15cb389cfe416ded9e52d01f'
                ],
            ]);

            $bodyContent = $response->getBody()->getContents();

            return view('emails.emailShow', ['body' => $bodyContent]);
        } catch (\Exception $e) {
            return back()->withErrors(['error' => 'Erro ao acessar o email: ' . $e->getMessage()]);
        }
    }


    public function uploadImage(Request $request)
    {
        if ($request->hasFile('upload')) {
            $originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_EXTENSION);
            $extension = $request->file('upload')->getClientOriginalExtension();

            $fileName = $fileName . '_' . time() . '.' . $extension;
            $request->file('upload')->move(public_path('media'), $fileName);

            $url = asset('media/' . $fileName);
            return response()->json(['fileName' => $fileName, 'uploaded' => 1, 'url' => $url]);
        }
    }
}
