<?php

namespace App\Http\Controllers;

use App\Models\Papel;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class UsersController extends Controller
{

    public readonly User $user;

    public function __construct()
    {
        $this->user = new User();
    }

    public function index(Request $request)
    {
        $users = $this->user->with('papel');

        if ($request->ajax()) {
            return DataTables::of($users)
                ->addColumn('profile_photo', function ($user) {
                    return asset('storage/' . $user->profile_photo_path);
                })
                ->addColumn('actions', function ($user) {
                    return view('admin.user.actions', compact('user'));
                })
                ->rawColumns(['profile_photo', 'actions'])
                ->make(true);
        }

        return view('admin.user.users');
    }



    public function create()
    {
        $papels = Papel::all();
        return view('admin.user.users_create', ['papels' => $papels]);
    }

    public function store(Request $request)
    {
        $nomecompleto = $request->input('nome') . ' ' . $request->input('sobrenome');

        if (!$this->user->where('name', $nomecompleto)->exists()) {
            $papelid = $request->input('papel_id');
            $data = 'profile-photos/profilePhotoDefault.png';

            if ($request->hasFile('profile_photo')) {
                $imageName = time() . '_' . $request->file('profile_photo')->getClientOriginalName();

                try {
                    $request->file('profile_photo')->move(public_path('storage/profile-photos'), $imageName);
                    $data = 'profile-photos/' . $imageName;
                    // dd($data);
                } catch (\Exception $e) {
                    return back()->withErrors(['file_error' => $e->getMessage()]);
                }
            }
            //dd($imageName);
            //dd($data);
            $criarUser = $this->user->create([
                'name' => $nomecompleto,
                'email' => $request->input('email'),
                'password' => Hash::make($request->input('senha')),
                'papel_id' => $papelid,
                'created_at' => now(),
                'updated_at' => now(),
                'profile_photo_path' => $data,
            ]);

            if ($criarUser) {
                return redirect()->route('users.index')->with('success', 'Usuário criado com sucesso');
            } else {
                return redirect()->route('users.index')->with('error', 'Erro ao criar usuário');
            }
        }
    }


    public function show(string $id) {}

    public function edit(string $id)
    {
        $user = $this->user->findOrFail($id);
        $papels = Papel::all();

        return view('admin.user.users_edit', ['user' => $user, 'papels' => $papels]);
    }

    public function update(Request $request, string $id)
    {
        $user = $this->user->findOrFail($id);

        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255|unique:users,email,' . $id,
            'password' => 'nullable|min:8|confirmed',
            'papel_id' => 'required|exists:papel,id',
            'profile-photo' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->papel_id = $request->input('papel_id');

        if ($request->filled('password')) {
            $user->password = Hash::make($request->input('password'));
        }

        if ($request->hasFile('profile_photo')) {
            $imageName = time() . '_' . $request->file('profile_photo')->getClientOriginalName();

            $request->file('profile_photo')->move(public_path('storage/profile-photos'), $imageName);

            if ($user->profile_photo_path && file_exists(public_path($user->profile_photo_path))) {
                unlink(public_path($user->profile_photo_path));
            }

            $user->profile_photo_path = 'profile-photos/' . $imageName;
        }

        if ($user->save()) {
            return redirect()->route('users.index')->with('success', 'Usuário atualizado com sucesso!');
        }

        return redirect()->back()->with('error', 'Erro ao atualizar usuário!');
    }

    public function destroy(string $id)
    {
        $user = $this->user->findOrFail($id);

        try {
            $user->delete();
            return redirect()->route('users.index')->with('success', 'Usuário excluído com sucesso');
        } catch (\Exception $e) {
            return redirect()->route('users.index')->with('error', 'Erro ao excluir o usuário');
        }
    }

  
}
