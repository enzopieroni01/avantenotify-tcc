<?php

namespace App\Http\Controllers;

use App\Events\MessageSent;
use App\Events\MessageSentEvent;
use App\Models\Notificacao;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Pusher\Pusher;

class NotificacaoController extends Controller
{
    public function index()
    {
        $users = User::where('id', '!=', Auth::id())->get();
        return view('chat', ['users' => $users]);
    }

    public function mobileChat()
    {
        $users = User::where('id', '!=', Auth::id())->get();
        return view('mobile.chat', ['users' => $users]);
    }


    public function autenticar(Request $request)
    {
        if (Auth::check()) {
            $pusher = new Pusher(
                'b497dd54a4f7569022ca',
                '6da988bdcbdc1d3e2b4b',
                '1857136',
                [
                    'cluster' => 'mt1',
                    'useTLS' => true
                ]
            );
            $auth = $pusher->socket_auth($request->channel_name, $request->socket_id);
            return response($auth, 200);
        } else {
            return response('Você precisa estar logado para acessar o chat - Inautorizado!', 403);
        }
    }

    public function sendMessage(Request $request)
    {
        if (!$request->has('usuario_destinatario_id') || !$request->usuario_destinatario_id) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'ID do destinatário não fornecido.'
            ]);
        }

        try {
            // Salva a mensagem no banco de dados
            $notificacao = Notificacao::create([
                'mensagem' => $request->message,
                'usuario_remetente_id' => Auth::id(),
                'usuario_destinatario_id' => $request->usuario_destinatario_id,
                'dataHora' => now(),
            ]);

            // Envia a mensagem pelo Pusher
            $pusher = new Pusher(
                'b497dd54a4f7569022ca',
                '6da988bdcbdc1d3e2b4b',
                '1857136',
                ['cluster' => 'mt1', 'useTLS' => true]
            );


            $data = [
                'message' => $notificacao->mensagem,
                'user_id' => Auth::id()
            ];

            // Dispara evento no canal
            $idChat = $request->idChat;
            $pusher->trigger('private-my-channel-' . $idChat, 'my-event', $data);

            return response()->json([
                'status' => 'sucesso',
                'mensagem' => 'Mensagem enviada com sucesso'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => $e->getMessage()
            ]);
        }
    }

    public function fetchMessages($usuario_destinatario_id)
    {
        $usuario_remetente_id = Auth::id();

        // busca as msgs entre o usuario logado e o destinatario
        $mensagens = Notificacao::where(function ($query) use ($usuario_remetente_id, $usuario_destinatario_id) {
            $query->where('usuario_remetente_id', $usuario_remetente_id)
                ->where('usuario_destinatario_id', $usuario_destinatario_id);
        })->orWhere(function ($query) use ($usuario_remetente_id, $usuario_destinatario_id) {
            $query->where('usuario_remetente_id', $usuario_destinatario_id)
                ->where('usuario_destinatario_id', $usuario_remetente_id);
        })->orderBy('dataHora', 'asc')->get();

        return response()->json($mensagens);
    }

    public function marcarNotificacoesComoLidas($selectedUserId)
    {
        try {
            // Obtém o ID do usuário logado
            $userLogado = Auth::id();

            // Verifique se o ID do usuário selecionado é válido
            if (!$selectedUserId) {
                return response()->json(['status' => 'erro', 'mensagem' => 'ID do usuário não fornecido.'], 400);
            }

            // Marca todas as notificações dessa conversa como lidas
            $notificacoes = Notificacao::where(function ($query) use ($selectedUserId, $userLogado) {
                // Busca notificações entre o remetente e o destinatário (e vice-versa)
                $query->where('usuario_remetente_id', $selectedUserId)
                    ->where('usuario_destinatario_id', $userLogado);
            })->where('lido', false) // Filtra para marcar somente as não lidas
                ->update(['lido' => true]); // Atualiza o status para 'lido'

            return response()->json(['status' => 'sucesso']);
        } catch (\Exception $e) {
            // Se algo der errado, retorna o erro no log e na resposta
            Log::error('Erro ao marcar notificações como lidas: ' . $e->getMessage());
            return response()->json(['status' => 'erro', 'mensagem' => 'Erro interno do servidor.'], 500);
        }
    }
}
