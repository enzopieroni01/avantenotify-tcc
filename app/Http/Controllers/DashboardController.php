<?php

namespace App\Http\Controllers;

use App\Models\Notificacao;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function main()
    {
        // Busca notificações não lidas para o usuário logado
        $notificacoesNaoLidas = Notificacao::where('usuario_destinatario_id', Auth::id())
            ->where('lido', false)
            ->orderBy('dataHora', 'desc')
            ->get();
        $idRemetenteNotificacao = Notificacao::where('usuario_remetente_id', Auth::id())->where('lido', false)
        ->orderBy('dataHora', 'desc')
        ->get('usuario_remetente_id');
        

        // Passa as notificações não lidas para a view
        return view('dashboard', [
            'notificacoesNaoLidas' => $notificacoesNaoLidas,
            'usuario' => $idRemetenteNotificacao,
        ]);
    }
}
