import { defineConfig } from "vite";
import laravel from "laravel-vite-plugin";

export default defineConfig({
  plugins: [
    laravel({
      input: ["resources/css/app.css", "resources/js/app.js"],
      refresh: true
    })
  ],
  env: {
    VITE_PUSHER_APP_KEY: process.env.PUSHER_APP_KEY,
    VITE_PUSHER_APP_CLUSTER: process.env.PUSHER_APP_CLUSTER
  },
  build: {
    outDir: 'dist', // Diretório de saída correto
},
});
