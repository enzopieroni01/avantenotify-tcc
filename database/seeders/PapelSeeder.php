<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PapelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('papel')->insert([
            [
                'nome' => 'User',
            ], [
                'nome' => 'Admin',
            ]
        ]);

        DB::table('Users')->insert([
            [
                'name' => 'Enzo Rodrigues Pieroni',
                'email' => 'enzopieroni01@gmail.com',
                'password' => bcrypt(123456789),
                'papel_id' => 2,
            ]
            ]);
    }
}
