<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('canal_notificacao', function (Blueprint $table) {
            $table->foreignId("notificacao_id")->constrained("notificacao");
            $table->foreignId("canal_id")->constrained("canal");
            $table->tinyInteger("lida");
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('canal_notificacao');
    }
};
