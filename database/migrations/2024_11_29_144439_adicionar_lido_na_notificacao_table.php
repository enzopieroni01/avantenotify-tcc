<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AdicionarLidoNaNotificacaoTable extends Migration

{
    public function up()
    {
        Schema::table('notificacao', function (Blueprint $table) {
            // Adiciona o campo `is_read`, com valor padrão de false
            $table->boolean('lido')->default(false)->after('dataHora');
        });
    }

    public function down()
    {
        Schema::table('notificacao', function (Blueprint $table) {
            // Remove o campo `is_read` caso seja necessário reverter
            $table->dropColumn('lido');
        });
    }
}

