@extends('layouts.main')
@section('title', 'Inbox')
@section('content')
    <x-app-layout>
        <style>
            #loader-overlay {
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background: rgba(255, 255, 255, 0.7);
                backdrop-filter: blur(5px);
                display: flex;
                justify-content: center;
                align-items: center;
                z-index: 9999;
            }
            .emailContainer{
                border: 1px solid rgba(0, 0, 0, 0.083);
                border-radius: 10px;
                height: 500px !important;
            }
            #loader img {
                width: 100px;
                height: 100px;
            }
            .material-symbols-outlined {
                font-size: 20px;
                margin-right: 10px;
            }

            .nav-item:hover {
                background-color: #bbb;
            }

            .email-list {
                max-height: 700px;
                overflow-y: auto;
                border: 1px solid #ddd;
                border-radius: 5px;
                padding: 10px;
                background-color: #f9f9f9;
            }
        </style>
        <div class="d-flex justify-content-center align-items-center min-h-screen">
            <div class="w-70 h-100 bg-white rounded  overflow-y-auto container" id="emailContainer" style="box-shadow: 10px 10px 20px rgba(20, 20, 20, 0.196);">
                <div class="email-app">
                    <nav>
                        <a href="{{ route('email') }}" class="btn btn-primary btn-block">Envio De Email <i
                                class="fa-solid fa-pen-to-square"></i></a>
                        <ul class="nav">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('inbox') }}"><span class="material-symbols-outlined" id="mailIcon">mail</span> Inbox <span class="badge text-bg-primary mt-1">{{ count($emails) }}</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#"><i class="fa fa-star"></i> Stared</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#"><i class="fa fa-rocket"></i> Sent</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#"><i class="fa fa-trash-o"></i> Trash</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#"><i class="fa fa-bookmark"></i> Important</a>
                            </li>
                        </ul>
                    </nav>
                    <main>
                        <div id="alert-area"></div>

                        <p class="text-center fs-4">
                            <strong>Caixa de Entrada <i class="fa fa-inbox"></i></strong>
                        </p>

                        <div class="email-list">
                            @if (count($emails) > 0)
                                <ul class="list-group">
                                    @foreach ($emails as $email)
                                        <li class="list-group-item">
                                            <strong>Assunto:</strong> {{ $email['subject'] }}<br>
                                            <strong>De:</strong> &lt;{{ $email['from_email'] }}&gt;<br>
                                            <strong>Para:</strong> {{ $email['to_name'] }}
                                            &lt;{{ $email['to_email'] }}&gt;<br>
                                            <strong>Data:</strong>
                                            {{ \Carbon\Carbon::parse($email['sent_at'])->format('d/m/Y H:i') }}<br>
                                            <a href="{{ route('showEmail', ['inboxId' => $email['inbox_id'], 'messageId' => $email['id']]) }}"
                                                class="btn btn-primary btn-sm mt-2">Ver Email <i
                                                    class="fa-solid fa-up-right-from-square"></i></a>
                                        </li>
                                    @endforeach
                                </ul>
                            @else
                                <div class="alert alert-danger" role="alert">
                                    <i class="fa-solid fa-triangle-exclamation"></i> Não há emails disponíveis na inbox.
                                </div>
                            @endif
                        </div>

                        <div id="loader-overlay" style="display: none;">
                            <div id="loader">
                                <img src="{{ asset('img/loader.gif') }}" alt="Carregando..." />
                            </div>
                        </div>
                    </main>
                </div>
            </div>
        </div>
    </x-app-layout>
@endsection

@section('scripts')
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    <script>
        var a = <?php echo count($emails); ?>;
        //console.log("tem "+ a + " emails");

        $(document).ready(function() {
            if(a > 0){
                $('#mailIcon').text('mark_email_unread');
            }
            else{
                $('#mailIcon').text('mail');
            }
        });
    </script>
@endsection
