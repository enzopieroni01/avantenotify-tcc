@extends('layouts.main')

@section('title', 'Corpo do Email')

@section('content')
<x-app-layout>
    <style>
        
        .container-email {
            display: flex;
            justify-content: center;
            align-items: center;
            min-height: 100vh;
        }

        .box-email{
            background-color: #fff;
            border-radius: 10px;
            box-shadow: 10px 10px 20px rgba(0, 0, 0, 0.196);
            padding: 20px;
            max-width: 1000px;
            width: 100%;
            text-align: center;
        }

        .header-email {
            display: flex;
            justify-content: space-between;
            align-items: center;
            margin-bottom: 20px;
        }

        .header-email h1 {
            font-size: 20px;
            font-weight: 600;
            color: #333;
            margin: 0;
        }

        .body-email {
            background-color: #fafafa;
            border: 1px solid #ddd;
            padding: 15px;
            border-radius: 5px;
            max-height: 600px;
            min-height: 500px;
            overflow-y: auto;
            font-size: 14px;
            line-height: 1.6;
            color: #444;
            margin-bottom: 20px;
        }

        .body-email::-webkit-scrollbar {
            width: 8px;
        }

        .body-email::-webkit-scrollbar-thumb {
            background-color: #bbb;
            border-radius: 10px;
        }

        .scroll-indicator {
            font-size: 12px;
            color: #999;
        }
    </style>

    <div class="container-email">
        <div class="box-email">
            <div class="header-email">
                <h1>Visualizar Email</h1>
                <a href="{{ route('inbox') }}" class=" btn btn-primary "><i class="fa-solid fa-arrow-left-long"> </i> Voltar para Inbox</a>
            </div>

            <div class="body-email">
                {!! $body !!}
            </div>
            <p class="scroll-indicator">Role para ver mais conteúdo</p>
        </div>
    </div>
</x-app-layout>
@endsection
