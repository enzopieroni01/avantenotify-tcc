@extends('layouts.main')
@section('title', 'Enviar Email')
@section('content')
    <x-app-layout>
        <style>
            #loader-overlay {
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background: rgba(255, 255, 255, 0.7);
                backdrop-filter: blur(5px);
                display: flex;
                justify-content: center;
                align-items: center;
                z-index: 9999;
            }

            #emailContainer {
                border: 1px solid rgba(0, 0, 0, 0.083);
                border-radius: 10px;
                height: 500px !important;
            }

            .material-symbols-outlined {
                font-size: 20px;
                margin-right: 10px;
            }

            #loader img {
                width: 100px;
                height: 100px;
            }

            .nav-link {
                display: flex;
                align-items: center;
            }

            .nav-item:hover {
                background-color: #bbb;
            }
        </style>
        <div class="d-flex justify-content-center align-items-center min-h-screen">
            <div class="w-70 h-100 bg-white rounded shadow-sm overflow-y-auto container" id="emailContainer">
                <div class="email-app">
                    <nav>
                        <a href="{{ route('email') }}" class="btn btn-primary btn-block">Envio De Email <i
                                class="fa-solid fa-pen-to-square"></i></a>
                        <ul class="nav">
                            <li class="nav-item">
                                <a class="nav-link" id="notificacao" href={{ route('inbox') }}>
                                    <span class="material-symbols-outlined" id="mailIcon">mail</span>
                                    Inbox <span class="badge badge-danger"></span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#"><i class="fa fa-star"></i> Stared</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#"><i class="fa fa-rocket"></i> Sent</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#"><i class="fa fa-trash-o"></i> Trash</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#"><i class="fa fa-bookmark"></i> Important</a>
                            </li>
                        </ul>
                    </nav>
                    <main>

                        <div id="alert-area"></div>

                        <p class="text-center fs-4"><strong>Envio de Email <i
                                    class="fa-solid fa-pen-to-square"></i></strong></p>
                        <form id="emailForm" action="{{ route('emailSend') }}" method="POST">
                            @csrf
                            @csrf
                            <div class="form-row mb-3">
                                <label for="to" class="col-2 col-sm-1 col-form-label">Para:</label>
                                <div class="col-10 col-sm-11">
                                    <input type="email" class="form-control" id="to" name="toEmail"
                                        placeholder="Destinatário">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-11 ">

                                    <div class="form-group mt-4">
                                        <div class="form-floating mb-2">
                                            {{-- <textarea class="form-control" placeholder="" name="conteudoEmail" id="floatingTextarea2" style="height: 100px"></textarea>
                                            <label for="floatingTextarea2">Corpo do Email</label> --}}
                                            <div id="editor">
                                            </div>


                                        </div>
                                        <div class="toolbar d-flex" role="toolbar">
                                            <div class="btn-group ml-auto">
                                                <div class="form-group mt-2 mr-auto">
                                                    <input type="submit" value="Enviar" class="btn btn-success">
                                                    <button class="btn btn-danger" type="button"
                                                        id="discartBtn">Descartar</button>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                </div>
                            </div>
                        </form>

                        <div id="loader-overlay" style="display: none;">
                            <div id="loader">
                                <img src="{{ asset('img/loader.gif') }}" alt="Enviando..." />
                            </div>
                        </div>
                    </main>
                </div>
            </div>
        </div>
    </x-app-layout>
@endsection

@section('scripts')
    <script></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script type="importmap">
        {
            "imports": {
                "ckeditor5": "https://cdn.ckeditor.com/ckeditor5/43.1.1/ckeditor5.js",
                "ckeditor5/": "https://cdn.ckeditor.com/ckeditor5/43.1.1/"
            }
        }
    </script>
    <script type="module">
        import {
            ClassicEditor,
            AccessibilityHelp,
            AutoImage,
            Autosave,
            BlockQuote,
            Bold,
            CKBox,
            CKBoxImageEdit,
            CloudServices,
            Essentials,
            FontBackgroundColor,
            FontColor,
            FontFamily,
            FontSize,
            Heading,
            ImageBlock,
            ImageCaption,
            ImageInline,
            ImageInsert,
            ImageInsertViaUrl,
            ImageResize,
            ImageStyle,
            ImageTextAlternative,
            ImageToolbar,
            ImageUpload,
            Indent,
            IndentBlock,
            Italic,
            LinkImage,
            Paragraph,
            PictureEditing,
            SelectAll,
            Underline,
            Undo
        } from 'ckeditor5';


        const editorConfig = {
            toolbar: {
                items: [
                    'undo',
                    'redo',
                    '|',
                    'heading',
                    '|',
                    'fontSize',
                    'fontFamily',
                    'fontColor',
                    'fontBackgroundColor',
                    '|',
                    'bold',
                    'italic',
                    'underline',
                    '|',
                    'insertImage',
                    'ckbox',
                    'blockQuote',
                    '|',
                    'outdent',
                    'indent'
                ],
                shouldNotGroupWhenFull: false
            },
            placeholder: 'Crie o corpo do email personalizado!',
            plugins: [
                AccessibilityHelp,
                AutoImage,
                Autosave,
                BlockQuote,
                Bold,
                CKBox,
                CKBoxImageEdit,
                CloudServices,
                Essentials,
                FontBackgroundColor,
                FontColor,
                FontFamily,
                FontSize,
                Heading,
                ImageBlock,
                ImageCaption,
                ImageInline,
                ImageInsert,
                ImageInsertViaUrl,
                ImageResize,
                ImageStyle,
                ImageTextAlternative,
                ImageToolbar,
                ImageUpload,
                Indent,
                IndentBlock,
                Italic,
                LinkImage,
                Paragraph,
                PictureEditing,
                SelectAll,
                Underline,
                Undo
            ],
            ckfinder: {
                uploadUrl: "{{ route('uploadImage', ['_token' => csrf_token()]) }}"
            },
            fontFamily: {
                supportAllValues: true
            },
            fontSize: {
                options: [10, 12, 14, 'default', 18, 20, 22],
                supportAllValues: true
            },
            heading: {
                options: [{
                        model: 'paragraph',
                        title: 'Paragraph',
                        class: 'ck-heading_paragraph'
                    },
                    {
                        model: 'heading1',
                        view: 'h1',
                        title: 'Heading 1',
                        class: 'ck-heading_heading1'
                    },
                    {
                        model: 'heading2',
                        view: 'h2',
                        title: 'Heading 2',
                        class: 'ck-heading_heading2'
                    },
                    {
                        model: 'heading3',
                        view: 'h3',
                        title: 'Heading 3',
                        class: 'ck-heading_heading3'
                    },
                    {
                        model: 'heading4',
                        view: 'h4',
                        title: 'Heading 4',
                        class: 'ck-heading_heading4'
                    },
                    {
                        model: 'heading5',
                        view: 'h5',
                        title: 'Heading 5',
                        class: 'ck-heading_heading5'
                    },
                    {
                        model: 'heading6',
                        view: 'h6',
                        title: 'Heading 6',
                        class: 'ck-heading_heading6'
                    }
                ]
            },
            image: {
                toolbar: [
                    'toggleImageCaption',
                    'imageTextAlternative',
                    '|',
                    'imageStyle:inline',
                    'imageStyle:wrapText',
                    'imageStyle:breakText',
                    '|',
                    'resizeImage',
                    '|',
                    'ckboxImageEdit'
                ]
            },
            ckfinder: {
                uploadUrl: "{{ route('uploadImage' , ['_token' => csrf_token()]) }}",
                
            }
        };

        let editorInstance;

        ClassicEditor
            .create(document.querySelector('#editor'), editorConfig)
            .then(editor => {
                editorInstance = editor;


            })
            .catch(error => {
                console.error(error);
            });

        function discart() {
            editorInstance.destroy();
        }

        document.getElementById('discartBtn').addEventListener('click', function() {
            if (editorInstance) {
                editorInstance.setData('');
            }
        });

        $(document).ready(function() {
            $('#emailForm').on('submit', function(event) {
                event.preventDefault();

                $('#loader-overlay').show();

                const conteudoEmail = editorInstance.getData();

                var formData = {
                    toEmail: $('#to').val(),
                    conteudoEmail: conteudoEmail,
                    _token: '{{ csrf_token() }}'
                };

                $.ajax({
                    type: 'POST',
                    url: $(this).attr('action'),
                    data: formData,
                    success: function(response) {
                        $('#loader-overlay').hide();

                        var successAlert =
                            '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
                            'Email enviado com sucesso!' +
                            '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>' +
                            '</div>';

                        $('#alert-area').html(successAlert);

                        $('#mailIcon').text('mark_email_unread');


                    },
                    error: function(xhr, status, error) {
                        $('#loader-overlay').hide();

                        var errorAlert =
                            '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                            'Erro ao enviar o email: ' + error +
                            '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>' +
                            '</div>';
                        $('#alert-area').html(errorAlert);
                    }
                });
            });
        });
    </script>
@endsection
