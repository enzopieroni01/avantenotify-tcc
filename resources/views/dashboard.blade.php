@extends('layouts.main')
@section('title', 'Dashboard')
@section('content')
    <style>
        .bg-off {
            background-color: #F7F9F9;
        }

        .profile-photo {
            width: 150px !important;
            height: 150px;
            border-radius: 50%;
            object-fit: cover;
            border: 4px solid #0669fc;
            display: block;
        }

        div::-webkit-scrollbar {
            width: 10px;
            /* Largura da barra */
        }

        /* Cor de fundo da barra */
        div::-webkit-scrollbar-track {
            background: #f1f1f1;
        }

        /* Cor do scroll */
        div::-webkit-scrollbar-thumb {
            background: #888;
            border-radius: 5px;
        }

        /* Cor do scroll ao passar o mouse */
        div::-webkit-scrollbar-thumb:hover {
            background: #555;
        }
    </style>
    <x-app-layout>
        <div class=" d-flex justify-content-center align-items-center min-h-screen">
            <div class="w-70 h-100 bg-white rounded shadow-sm overflow-y-auto container d-flex justify-content-center">
                <div
                    class="row  d-flex bg-white overflow-hidden shadow-xl sm:rounded-lg border border-solid hover:border-none">
                    <div class="col my-5">
                        <div class="d-flex justify-content-center align-items-center">
                            <div class="row">
                                <div class="col-3 p-0 d-flex justify-content-center">
                                    @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())
                                        <img class="profile-photo" src="{{ Auth::user()->profile_photo_url }}"
                                            alt="{{ Auth::user()->name }}" />
                                    @else
                                        <span class="inline-flex rounded-md">
                                            <button type="button"
                                                class="btn-user inline-flex items-center px-3 py-2 text-sm leading-4 font-medium bg-white hover:text-blue-500 focus:outline-none focus:bg-gray-50 active:bg-gray-50 transition ease-in-out duration-150 align-self-center">
                                                {{ Auth::user()->name }}
                                                <svg class="ms-2 -me-0.5 h-4 w-4" xmlns="http://www.w3.org/2000/svg"
                                                    fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                                    stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round"
                                                        d="M19.5 8.25l-7.5 7.5-7.5-7.5" />
                                                </svg>
                                            </button>
                                        </span>
                                    @endif
                                </div>
                                <div class="col p-0 d-flex align-items-center">
                                    <h1 class="text-center fs-3"><strong>Seja Bem-Vindo :</strong> {{ Auth::user()->name }}
                                    </h1>
                                </div>
                                <div class="row mt-4">
                                    <div class="col mx-5">
                                        <div class="card">
                                            <div class="card-header">
                                                <h1 class="fs-5 text-primary"><strong>Dados</strong></h1>
                                            </div>
                                            <div class="card-body">
                                                <div class="mb-3">
                                                    <p class="text-secondary text-sm">Nome :</p>
                                                    <h2 class="ms-2 pb-3 border-bottom p-1">
                                                        <i class="fa-regular fa-user me-1 text-primary"></i>
                                                        {{ Auth::user()->name }}
                                                    </h2>
                                                </div>
                                                <div class="mb-3">
                                                    <p class="text-secondary text-sm">Email :</p>
                                                    <h2 class="ms-2 pb-3 border-bottom p-1">
                                                        <i class="fa-solid fa-at me-1 text-primary"></i>
                                                        {{ Auth::user()->email }}
                                                    </h2>
                                                </div>
                                                <div>
                                                    <p class="text-secondary text-sm">Notificações Não Lidas :</p>
                                                    <h2 class="ms-2 pb-3 border-bottom p-1">
                                                        <i class="fa-regular fa-bell me-1 text-primary"></i>
                                                        {{ count($notificacoesNaoLidas) }}
                                                    </h2>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-5 mt-5 mx-5">
                        <h1 class="fs-4 mb-2 mt-3"><strong><i class="fa-regular fa-bell text-primary"></i>
                                Notificações</strong></h1>
                        <div class="card text-center">
                            <div class="card-header">
                                <ul class="nav nav-pills card-header-pills mt-2">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#recentes" data-toggle="tab">Recentes <i
                                                class="fa-solid fa-clock-rotate-left"></i></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#favoritas" data-toggle="tab">Favoritas <i
                                                class="fa-regular fa-star"></i></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#deletadas" data-toggle="tab">Deletadas <i
                                                class="fa-solid fa-recycle"></i></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" style="height: 300px; overflow-y: auto;">
                                    <div class="tab-pane fade show active" id="recentes">
                                        @if ($notificacoesNaoLidas->count() > 0)
                                            @foreach ($notificacoesNaoLidas as $notificacao)
                                                <div class="toast show" role="alert" aria-live="assertive"
                                                    aria-atomic="true">
                                                    <div class="toast-header">
                                                        <img src="{{ asset('storage/' . $notificacao->remetente->profile_photo_path) }}"
                                                            class="rounded me-2" alt="Remetente" class="rounded me-2"
                                                            width="90">
                                                        <strong class="me-auto">{{ $notificacao->remetente->name }}</strong>
                                                        <small>{{ \Carbon\Carbon::parse($notificacao->dataHora)->diffForHumans() }}</small>
                                                        <button type="button" class="btn-close" data-bs-dismiss="toast"
                                                            aria-label="Close"></button>
                                                    </div>
                                                    <div class="toast-body">
                                                        {{ $notificacao->mensagem }}
                                                    </div>
                                                </div>
                                            @endforeach
                                        @else
                                            <div
                                                class=" tab-pane text-center py-3 d-flex align-items-center justify-content-center">
                                                <i class="fa-solid fa-clock-rotate-left"></i> Nenhuma Notificação Recente.
                                            </div>
                                        @endif
                                    </div>

                                    <div class="tab-pane fade" id="favoritas">
                                        <i class="fa-regular fa-star"></i> Nenhuma Notificação Favoritada.
                                    </div>
                                    <div class="tab-pane fade" id="deletadas">
                                        <i class="fa-solid fa-recycle"></i> Nenhuma Notificação Deletada.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </x-app-layout>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.nav-link').click(function(e) {
                e.preventDefault();
                $('.nav-link').removeClass('active');
                $(this).addClass('active');
                var contentId = $(this).attr('href');
                $('.tab-pane').removeClass('show active');
                $(contentId).addClass('show active');
            });
            document.addEventListener('DOMContentLoaded', function() {
                var toastElList = [].slice.call(document.querySelectorAll('.toast'))
                var toastList = toastElList.map(function(toastEl) {
                    return new bootstrap.Toast(toastEl, {
                        autohide: false
                    });
                });
                toastList.forEach(toast => toast.show());
            });

        });
    </script>
@endsection
