@extends('layouts.main')
@section('title', 'Chat')
@section('styles')
    <style>
        .data {
            display: inline-block;
            font-size: 1%;
        }

        .container {
            display: flex;
            height: 100%;
            padding: 0;
        }

        .user-list {
            width: 25%;
            border-right: 1px solid #ddd;
            overflow-y: auto;
            background-color: #ededed96;
            padding: 10px;
            border-radius: 10px;
            box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);

        }

        .chat-window {
            width: 75%;
            display: flex;
            flex-direction: column;
            padding: 10px;
        }

        .messages {
            flex: 1;
            padding: 15px;
            overflow-y: auto;
            border-bottom: 1px solid #ddd;
            max-height: 60vh;
            background-color: #f8f9fa;
            background-image: url('img/chatBg.png');
            background-size: cover;
            background-position: center;
            background-repeat: no-repeat;
            min-height: 60vh;
            border-radius: 10px;
            box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
        }


        .messages::before {
            content: "";
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-image: url('img/chatBg.png');
            background-size: cover;
            background-position: center;
            background-repeat: repeat;
            opacity: 0.8;
            z-index: -1;
            border-radius: 10px;
        }

        table.dataTable.row-border tbody th,
        table.dataTable.row-border tbody td,
        table.dataTable.display tbody th,
        table.dataTable.display tbody td {
            border-top: none !important;
        }

        .messages::-webkit-scrollbar {
            width: 8px;
        }

        .messages::-webkit-scrollbar-track {
            background: #f1f1f1;
            border-radius: 10px;
        }

        .messages::-webkit-scrollbar-thumb {
            background: #007bff49;
            border-radius: 10px;
        }

        .messages::-webkit-scrollbar-thumb:hover {
            background: #0057b3c3;
        }

        .message {
            display: block;
            margin-bottom: 10px;
            padding: 10px 15px;
            border-radius: 15px;
            min-width: 57px;
            max-width: 50%;
            word-wrap: break-word;
            width: max-content;
            box-shadow: 0 3px 6px rgba(0, 0, 0, 0.1);
        }

        .message.sent small {
            font-size: 65%;
            text-align: right;
            display: block;
        }

        .message.received small {
            font-size: 65%;
            text-align: left;
            display: block;
        }

        .message.sent small svg {
            display: inline;
            margin-left: 2px;
        }

        .sent {
            background-color: #007bff;
            color: white;
            align-self: flex-end;
            border-end-end-radius: 0;
        }

        .received {
            background-color: #d2d2d2;
            color: #333;
            align-self: flex-start;
            border-end-start-radius: 0;
        }

        .message.sent {
            margin-left: auto;
            margin-right: 10px;
        }

        .message.received {
            margin-right: auto;
            margin-left: 10px;
        }

        .messages-input {
            padding: 15px;
            display: flex;
            background-color: #fff;
            border-top: 1px solid #ddd;
            border-radius: 0 0 10px 10px;
        }

        .messages-input textarea {
            flex: 1;
            margin-right: 10px;
            border-radius: 10px;
            padding: 10px;
            box-shadow: inset 0 2px 5px rgba(0, 0, 0, 0.1);
            border: 1px solid #ddd;
        }

        .messages-input textarea:focus {
            border-color: #007bff;
            outline: none;
            box-shadow: inset 0 2px 5px rgba(0, 0, 0, 0.2);
        }

        #send {
            background-color: #007bff;
            width: 65px;
            border: none;
            padding: 10px;
            color: white;
            display: flex;
            justify-content: center;
            align-items: center;
            box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
            transition: background-color 0.2s;
        }

        #send:hover {
            background-color: #0056b3;
        }

        .user-list-item {
            cursor: pointer;
            padding: 10px 15px;
            border-bottom: 1px solid #b2b2b2;
            display: flex;
            justify-content: flex-start;
            align-items: center;
            background-color: #fff;
            border-radius: 10px;
            margin-bottom: 5px;
            transition: background-color 0.2s, box-shadow 0.2s;
        }

        .user-list-item:hover {
            background-color: #f1f1f1;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        }

        .user-list-item.active {
            background-color: #007bff;
            color: white;
            box-shadow: 0 4px 10px rgba(0, 0, 0, 0.1);
        }

        .user-list-item.active .user-name {
            color: white;
        }

        .hiddenTitle {
            display: none;
        }

        .profile-photo {
            width: 40px;
            height: 40px;
            border-radius: 50%;
            object-fit: cover;
            display: inline;
            margin-right: 10px;
        }

        .user-name {
            font-size: 16px;
            font-weight: bold;
            text-align: left;
            max-width: 150px;
            display: inline-block;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }

        .cardChat {
            border-radius: 10px;
            box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
            background-color: #fff;
        }

        #tituloChat {
            padding: 10px;
            font-size: 20px;
            font-weight: bold;
            background-color: #f1f1f1;
            border-radius: 10px;
        }

        .profile-photo {
            width: 40px;
            height: 40px;
            border-radius: 50%;
        }

        .d-flex {
            display: flex;
            align-items: center;
        }

        .me-2 {
            margin-right: 8px;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button {
            display: inline-block;
            padding: 8px 12px;
            margin: 0 2px;
            border: 1px solid #ddd;
            border-radius: 5px;
            background-color: #f8f9fa;
            color: #333;
            font-size: 14px;
            text-align: center;
            cursor: pointer;
            transition: all 0.2s ease-in-out;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button.current {
            background-color: #007bff;
            color: #fff;
            border: 1px solid #007bff;
            box-shadow: 0 4px 10px rgba(0, 0, 0, 0.1);
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
            background-color: #0056b3 !important;
            color: #fff;
            border-color: #0056b3;
            box-shadow: 0 3px 8px rgba(0, 0, 0, 0.2);
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button.disabled {
            background-color: #e9ecef;
            color: #6c757d;
            border-color: #ddd;
            cursor: not-allowed;
            box-shadow: none;
        }

        .dataTables_wrapper .dataTables_paginate {
            display: flex;
            justify-content: center;
            align-items: center;
            gap: 5px;
            margin-top: 10px;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button {
            display: inline-block;
            padding: 6px 12px;
            font-size: 14px;
            color: #007bff;
            background-color: #ffffff;
            border: 1px solid #007bff;
            border-radius: 4px;
            cursor: pointer;
            transition: all 0.3s ease-in-out;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button.current {
            background-color: #007bff;
            color: #ffffff;
            font-weight: bold;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
            background-color: #0056b3;
            color: #ffffff;
            border-color: #0056b3;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button.disabled {
            background-color: #e9ecef;
            color: #6c757d;
            cursor: not-allowed;
        }


        .dataTables_wrapper .dataTables_paginate .paginate_button {
            min-width: 35px;
            text-align: center;
        }

        #userTable_wrapper>div:nth-child(3) {
            width: 100%;
        }

        #userTable_paginate {
            width: 250px;
        }

        .user-list {
            overflow-x: hidden;
        }

        #userTable_filter>label>input {
            width: 300px !important;
        }

        #userTable_filter>label {
            text-align: left !important;
        }

        #userTable_filter {
            float: right !important;
            margin-bottom: 20px !important;
        }

        #userTable_filter input {
            border-radius: 10px !important;
            padding: 10px 30px !important;
            border: 2px solid #ccc !important;
            font-size: 16px !important;
            width: 250px !important;
            box-sizing: border-box !important;
            background-color: #ffffff
        }

        #userTable_filter input::placeholder {
            color: #999 !important;
        }

        #userTable_paginate>ul>li.paginate_button.page-item.active {
            background-color: #168cfb82 !important;
            border-radius: 10px;
        }

        #userTable_paginate>ul>li.paginate_button.page-item {
            border-radius: 10px;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
            color: white !important;
            border: 1px solid #111 !important;
            background-color: #168cfb82 !important;

        }

        .user-name {
            vertical-align: middle;
        }

        #userTable tbody tr {
            background-color: #f8f9fa;
            margin-top: 2px;
        }

        #userTable tbody tr:hover {
            background-color: #0e86ffdc;

        }

        #userTable>tbody>tr.user-list-item.active {
            background-color: #0a82fadc !important;
            color: #fff !important;
        }

        .messages {
            position: relative;
            width: 100%;
            max-width: 1200px;
            margin: 0 auto;
            padding: 20px;
            background-color: rgba(255, 255, 255, 0.8);
            border-radius: 8px;
            overflow-y: auto;
            height: 60vh;
        }

        .imgbox {
            width: 100%;
            height: 500px;
            overflow: hidden;
            border-radius: 8px;
        }

        .imgbox img {
            width: 100%;
            height: 100%;
            object-fit: cover;
        }

        @media (max-width: 600px) {
            .messages {
                height: 50vh;
                padding: 10px;
            }

            .imgbox {
                height: 150px;
            }
        }

        @media (min-width: 1200px) {
            .messages {
                padding: 30px;
                max-width: 1400px;
                height: 80vh;
            }

            .imgbox {
                height: 700px;
            }

            .imgbox img {
                height: 100%;
            }
        }
    </style>

@endsection
@section('content')
    <x-app-layout>
        <div class="d-flex justify-content-center align-items-center min-h-screen">
            <div class="container-xxl shadow-sm p-3 mb-5 bg-body-secondary rounded">
                <div class="container chatPusher">
                    <div class="user-list me-2">
                        <table id="userTable" class="display nowrap" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th>Usuários</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    <tr class="user-list-item" data-id="{{ $user->id }}" data-nome="{{ $user->name }}">
                                        <td>
                                            <img src="{{ $user->profile_photo_path ? asset('storage/' . $user->profile_photo_path) : asset('img/profilePhotoDefault.png') }}"
                                                class="profile-photo me-2">
                                            <span class="user-name">{{ $user->name }}</span>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="chat-window">
                        <div class="cardChat">
                            <div id="chat-header" class="card-header">
                                <b class="fs-4">Chat via Push <i class="fa-regular fa-comments"></i></b>
                            </div>
                            <div class="card-header hiddenTitle" id="tituloChat"></div>
                            <div class="messages card-body" id="messages">
                                <div class="imgbox">
                                    <img src="{{ asset('img/chatBgMain1.png') }}">
                                </div>
                            </div>
                            <div class="messages-input card-footer">
                                <textarea id="message-input" class="form-control" placeholder="Mensagem"></textarea>
                                <button id="send" class="btn btn-primary">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="2em" height="2em"
                                        viewBox="0 0 24 24">
                                        <g fill="none">
                                            <path fill="currentColor"
                                                d="m7.24 4.535l11.944 5.658c1.525.722 1.525 2.892 0 3.614L7.24 19.466c-1.415.67-3.017-.472-2.844-2.028l.58-5.216a2 2 0 0 0 0-.442l-.58-5.216c-.173-1.557 1.429-2.7 2.844-2.029"
                                                opacity="0.16" />
                                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                                                stroke-width="2"
                                                d="m5 12l-.604-5.437C4.223 5.007 5.825 3.864 7.24 4.535l11.944 5.658c1.525.722 1.525 2.892 0 3.614L7.24 19.466c-1.415.67-3.017-.472-2.844-2.028zm0 0h7" />
                                        </g>
                                    </svg>
                                </button>
                            </div>
                            <div id="response"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </x-app-layout>
@endsection

@section('scripts')
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.6/js/dataTables.bootstrap5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
    <script src="https://js.pusher.com/8.2.0/pusher.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script type="module">
        document.addEventListener("DOMContentLoaded", function() {
            Pusher.logToConsole = true;
            let selectedUserId = null;
            let selectedUserName = null;
            let userLogado = "{{ Auth::user()->id }}";
            let idChatStr = null;

            // Configuração do Pusher
            var pusher = new Pusher("{{ env('PUSHER_APP_KEY') }}", {
                cluster: "{{ env('PUSHER_APP_CLUSTER') }}",
                encrypted: true,
                authEndpoint: '{{ route('chat.auth') }}',
                auth: {
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    }
                }
            });

            // Configuração da DataTable
            const userTable = $('#userTable').DataTable({
                paging: true,
                pageLength: 9,
                lengthChange: false,
                searching: true,
                ordering: false,
                info: false,
                language: {
                    paginate: {
                        previous: "<i class='fa fa-arrow-left'></i>",
                        next: "<i class='fa fa-arrow-right'></i>"
                    },
                    search: "",
                    zeroRecords: "Nenhum usuário encontrado"
                }
            });
            $('#userTable_filter > label > input').attr('placeholder', "🔎 Buscar Usuários... ");
            $('#userTable_filter > label > input').css({
                'font-size': '14px',
                'border-radius': '25px',
                'padding': '10px 30px',
                'border': '2px solid #ccc',
            });

            // Seleção de usuário e chat
            $('#userTable tbody').on('click', '.user-list-item', function() {
                selectedUserName = $(this).data('nome');
                selectedUserId = $(this).data('id');

                $('#tituloChat').removeClass('hiddenTitle');
                $('#tituloChat').html('<b class="fs-5"> Chat com ' + selectedUserName + '</b>');

                $('.messages').html('');
                $('.user-list-item').removeClass('active');
                $(this).addClass('active');

                let arr = [selectedUserId, userLogado];
                arr.sort();
                idChatStr = arr.join("-");

                // Marcar notificações como lidas
                $.ajax({
                    url: '/notificacao/ler/' + selectedUserId,
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(response) {
                        if (response.status == 'sucesso') {
                            console.log('Notificações marcadas como lidas.');
                        } else {
                            console.log('Erro ao marcar notificações como lidas.');
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.error('Erro ao marcar notificações como lidas:', textStatus);
                    }
                });

                // Carregar mensagens anteriores
                $.ajax({
                    url: '/chat/messages/' + selectedUserId,
                    method: 'GET',
                    success: function(mensagens) {
                        mensagens.forEach(function(mensagem) {
                            let messageClass = mensagem.usuario_remetente_id ==
                                userLogado ? 'sent' : 'received';
                            $('#messages').append(
                                '<div class="message ' + messageClass + '"><span>' +
                                mensagem.mensagem +
                                '</span> <br><small class="text-muted">' +
                                moment(mensagem.dataHora).format('dddd, HH:mm') +
                                '</small></div>'
                            );
                        });
                        scrollToBottom();
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.error('Erro ao buscar mensagens antigas:', textStatus);
                    }
                });

                // Pusher para escutar mensagens em tempo real
                const channel = pusher.subscribe('private-my-channel-' + idChatStr);
                channel.bind('my-event', function(data) {
                    let messageClass = data.user_id == userLogado ? 'sent' : 'received';
                    $('#messages').append(
                        '<div class="message ' + messageClass + '"><span>' + data.message +
                        '</span> <br><small class="text-muted">' + moment().format(
                            'dddd, HH:mm') + '</small></div>'
                    );
                    if (selectedUserId && selectedUserId == data.user_id) {
                        $.ajax({
                            url: '/notificacao/ler/' + selectedUserId,
                            method: 'POST',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function(response) {
                                if (response.status == 'sucesso') {
                                    console.log('Notificações marcadas como lidas.');
                                } else {
                                    console.log(
                                        'Erro ao marcar notificações como lidas.');
                                }
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                console.error('Erro ao marcar notificações como lidas:',
                                    textStatus);
                            }
                        });
                    }
                });
            });

            $(document).on('click', '#send', function() {
                let message = $('#message-input').val();

                if (!selectedUserId) {
                    Swal.fire({
                        title: "Ops!",
                        text: "Escolha um usuário para enviar uma mensagem 😅",
                        icon: "warning",
                        confirmButtonText: "OK"
                    });
                    return;
                }

                if (message.trim()) {
                    $('#send').html('<small><i class="fa fa-spinner fa-spin"></i> Enviando...<small>');

                    $.ajax({
                        url: '{{ route('chat.send') }}',
                        type: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {
                            'message': message,
                            'idChat': idChatStr,
                            'usuario_destinatario_id': selectedUserId
                        },
                        success: function(resposta) {
                            if (resposta.status == 'sucesso') {
                                console.log(resposta.mensagem);
                            } else {
                                $('#response').html('<p style="color:red;">' + resposta
                                    .mensagem + '</p>');
                            }
                            $('#send').html(`
                                        <svg xmlns="http://www.w3.org/2000/svg" width="2em" height="2em" viewBox="0 0 24 24">
                                            <g fill="none">
                                                <path fill="currentColor" d="m7.24 4.535l11.944 5.658c1.525.722 1.525 2.892 0 3.614L7.24 19.466c-1.415.67-3.017-.472-2.844-2.028l.58-5.216a2 2 0 0 0 0-.442l-.58-5.216c-.173-1.557 1.429-2.7 2.844-2.029" opacity="0.16" />
                                                <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m5 12l-.604-5.437C4.223 5.007 5.825 3.864 7.24 4.535l11.944 5.658c1.525.722 1.525 2.892 0 3.614L7.24 19.466c-1.415.67-3.017-.472-2.844-2.028zm0 0h7" />
                                            </g>
                                        </svg>
                                        `);
                            scrollToBottom();
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            $('#response').html(
                                '<p style="color:red;">Erro ao enviar mensagem: ' +
                                textStatus + '</p>');
                            $('#send').html(`
                                        <svg xmlns="http://www.w3.org/2000/svg" width="2em" height="2em" viewBox="0 0 24 24">
                                            <g fill="none">
                                                <path fill="currentColor" d="m7.24 4.535l11.944 5.658c1.525.722 1.525 2.892 0 3.614L7.24 19.466c-1.415.67-3.017-.472-2.844-2.028l.58-5.216a2 2 0 0 0 0-.442l-.58-5.216c-.173-1.557 1.429-2.7 2.844-2.029" opacity="0.16" />
                                                <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m5 12l-.604-5.437C4.223 5.007 5.825 3.864 7.24 4.535l11.944 5.658c1.525.722 1.525 2.892 0 3.614L7.24 19.466c-1.415.67-3.017-.472-2.844-2.028zm0 0h7" />
                                            </g>
                                        </svg>
                                        `);
                            scrollToBottom();
                        }
                    });


                    $('#message-input').val("");

                } else {
                    Swal.fire({
                        title: "Cadê a Mensagem?",
                        text: "Escreva uma mensagem 😅​",
                        icon: "question"
                    });
                }
            });

            function scrollToBottom() {
                var messages = document.getElementById('messages');
                if (messages) {
                    messages.scrollTop = messages.scrollHeight;
                }
            }
        });
    </script>
@endsection
