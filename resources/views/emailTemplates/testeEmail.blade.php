<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Email Avante Notify</title>
    <link
        href="https://fonts.googleapis.com/css2?family=Inter:wght@100..900&family=Ubuntu:ital,wght@0,300;0,400;0,500;0,700;1,300;1,400;1,500;1,700&display=swap"
        rel="stylesheet">
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            margin: 0;
            padding: 0;
            color: #333333;
        }

        .email-container {
            max-width: 600px;
            margin: 20px auto;
            background-color: #ffffff;
            border-radius: 8px;
            box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
            overflow: hidden;
        }

        .header {
            background-color: #0D6EFD;
            color: white;
            text-align: center;
            padding: 20px 0;
            font-size: 24px;
            font-family: "Ubuntu", sans-serif;
            display: flex !important;
            justify-content: center !important;
            align-items: center !important;
        }

        .header img {
            max-width: 50px;
            vertical-align: middle;
        }

        .content {
            padding: 20px;
            text-align: left;
        }

        .content p {
            font-size: 16px;
            line-height: 1.6;
        }

        .content .highlight {
            background-color: #e7e7f3;
            border-left: 4px solid #055ee5;
            padding: 10px;
            margin: 20px 0;
            font-style: italic;
        }

        .footer {
            background-color: #f4f4f4;
            padding: 10px 20px;
            text-align: center;
            font-size: 12px;
            color: #666666;
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;
        }

        .footer img {
            max-width: 20px;
            vertical-align: middle;
            margin-right: 5px;
        }

        .footer p {
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: row;
        }

        #conteudo {
            overflow-wrap: break-word;
            text-align: start;
            white-space: normal;
        }
    </style>
</head>

<body>
    <div class="email-container">
        <div class="header">
            <img src="{{ asset('img/favicon.png') }}" alt="Avante Logo">
            <p>Avante Notify Mail</p>
        </div>
        <div class="content">
            <p>Olá, {{ $emailDestinatario }}</p>
            <span id="conteudo"><h1>oi</h1>{!! $conteudoEmail !!}</span>
            <div class="highlight">
                <p><strong>Nota:</strong> Esta é uma mensagem automática. Por favor, não responda este e-mail.</p>
            </div>
            <p>Atenciosamente,<br>Equipe Avante Notify</p>
        </div>
        <div class="footer">
            <p><img src="{{ asset('img/logoIcon.svg') }}" alt="Avante Logo" width="15%"> Sistema de Mensageria e
                Notificações Avante Notify</p>
            <p>Avante Notify &copy; 2024</p>
        </div>
    </div>
</body>

</html>
