@extends('layouts.main')

@section('title', 'Gerenciar Usuários')

@section('content')
    <style>
        body {
            background-color: #f9f9f9;
            font-family: 'Arial', sans-serif;
            margin: 0;
            padding: 0;
            height: 100vh;
        }

        main {
            display: flex;
            justify-content: center;
            align-items: center;
            width: 100%;
            height: 100%;
        }

        .container {
            background: #ffffff;
            border-radius: 10px;
            padding: 20px;
            box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
            max-width: 800px;
            width: 100%;
        }

        .page-header {
            display: flex;
            justify-content: space-between;
            align-items: center;
            margin-bottom: 20px;
        }

        .page-header h1 {
            font-size: 1.8rem;
            color: #333;
        }

        .btn-primary a {
            color: #ffffff;
            text-decoration: none;
        }

        .btn-primary a:hover {
            color: #ffffff;
            text-decoration: underline;
        }

        .table {
            width: 100%;
            background-color: #ffffff;
            margin-top: 10px;
            border-collapse: collapse;
        }

        .table thead {
            background-color: #007bff;
            color: #ffffff;
        }

        .table th {
            padding: 10px;
            text-align: left;
            font-size: 1rem;
        }

        .table tbody tr {
            border-bottom: 1px solid #ddd;
        }

        .table tbody tr:hover {
            background-color: #f1f1f1;
        }

        .table td {
            padding: 10px;
            vertical-align: middle;
        }

        .profile-photo {
            width: 50px;
            height: 50px;
            border-radius: 50%;
            object-fit: cover;
            border: 2px solid #ddd;
            display: block;
        }

        .btn-action {
            width: 70px;
            height: 40px;
            padding: 5px;
        }

        select {
            width: 55px;
        }

        #users-table_paginate>span>a {
            border: 2px solid #007bff !important;
            border-radius: 50px !important;
            background-color: #007bffb9 !important;
            color: #000000 !important;
            cursor: pointer !important;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button .next{
            background-color: #007bff;
            color: #fff;
            border: none;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button:hover{
            background-color: #007bff !important;
            color: #fff !important;
        }   
    </style>

    <x-app-layout>
        <div class="container lista">
            <div class="page-header">
                <h1>Gerenciar Usuários</h1>
                <button class="btn btn-primary">
                    <a href="{{ route('users.create') }}">Criar Usuário <span><i class="fa-solid fa-user-plus"></i></span></a>
                </button>
            </div>

            <table class="table" id="users-table">
                <thead>
                    <tr>
                        <th scope="col">Perfil</th>
                        <th scope="col">Nome</th>
                        <th scope="col">Email</th>
                        <th scope="col">Opções</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </x-app-layout>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('#users-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ route('users.index') }}',
                columns: [{
                        data: 'profile_photo_path',
                        name: 'profile_photo_path',
                        render: function(data, type, row) {
                            if (!data) {
                                return '<img src="storage/profile-photos/profilePhotoDefault.png" class="profile-photo" />';
                            }

                            return '<img src="storage/' + data + '" class="profile-photo" />';
                        }
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'actions',
                        name: 'actions',
                        orderable: false,
                        searchable: true,
                        render: function(data, type, row) {
                            return `<button onclick="window.location.href='/users/${row.id}/edit'" class="btn btn-action btn-primary">Editar</button>
                                    <button type="button" class="btn btn-action btn-danger" onclick="confirmDeletion(${row.id})">Excluir</button>`;
                        }




                    }
                ],

                language: {
                    url: 'https://cdn.datatables.net/plug-ins/1.10.24/i18n/pt_br.json'
                },
                initComplete: function() {
                    const searchInput = $('div.dataTables_filter input');
                    const btnInput = $('paginate_button');

                    searchInput.addClass('form-control');
                    btnInput.removeClass('paginate_button');
                    btnInput.addClass('btn btn-primary form-control');

                    searchInput.attr('placeholder', 'Pesquisar usuários...');

                    searchInput.css({
                        'border': '2px solid #007BFF',
                        'border-radius': '5px',
                        'padding': '5px'
                    });
                }

            });
        });

        function confirmDeletion(userId) {
            Swal.fire({
                title: 'Você tem certeza?',
                text: "Esta ação não pode ser desfeita!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sim, excluir!',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.isConfirmed) {
                    const form = document.createElement('form');
                    form.action = `/users/${userId}`;
                    form.method = 'POST';

                    const csrfInput = document.createElement('input');
                    csrfInput.type = 'hidden';
                    csrfInput.name = '_token';
                    csrfInput.value = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

                    const methodInput = document.createElement('input');
                    methodInput.type = 'hidden';
                    methodInput.name = '_method';
                    methodInput.value = 'DELETE';

                    form.appendChild(csrfInput);
                    form.appendChild(methodInput);

                    document.body.appendChild(form);
                    form.submit();
                }
            });
        }
    </script>
@endsection
