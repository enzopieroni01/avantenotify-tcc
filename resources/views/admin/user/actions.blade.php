<a class="btn btn-sm btn-primary" href="{{ route('users.edit', $user->id) }}" role="button">Editar</a>
<button type="button" class="btn btn-sm btn-danger" onclick="event.preventDefault(); document.getElementById('delete-user-form-{{ $user->id }}').submit()">
    Deletar
</button>
<form id="delete-user-form-{{ $user->id }}" action="{{ route('users.destroy', $user->id) }}" method="POST" style="display: none;">
    @csrf
    @method('DELETE')
</form>
