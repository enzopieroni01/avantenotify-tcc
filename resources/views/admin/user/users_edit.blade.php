@extends('layouts.main')
@section('title', 'Editar Usuário')
@section('content')
    <style>
        .profile-photo-container {
            position: relative;
            width: 100px;
            height: 100px;
            margin: 0 auto;
        }

        .profile-photo {
            width: 100px;
            height: 100px;
            border-radius: 50%;
            object-fit: cover;
            border: 2px solid #055fe5c5;
            display: block;
        }

        .edit-icon {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            background-color: rgba(0, 0, 0, 0.44);
            color: white;
            border-radius: 50%;
            padding: 10px;
            cursor: pointer;
            font-size: 18px;
            display: none;
            width: 100px;
            height: 100px;
        }

        .profile-photo-container:hover .edit-icon {
            display: block;
        }

        .edit-icon i {
            position: relative;
            margin-top: 42%;
        }

        .form-label {
            font-weight: bold;
        }

        #profile_photo {
            display: none;
        }
    </style>

    <x-app-layout>
        <div class="d-flex justify-content-center align-items-center min-h-screen">
            <div class="container-xxl shadow-sm p-4 mb-5 bg-body-secondary rounded">
                <h1 class="text-center fs-4">Editar Usuário</h1>

                <div class="card p-4">
                    <form action="{{ route('users.update', $user->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <div class="mb-3 text-center">
                            <label for="profile_photo" class="form-label">Alterar Foto de Perfil</label>
                            <div class="profile-photo-container">
                                <img id="profilePreview" 
                                     src="{{ $user->profile_photo_path ? asset('storage/' . $user->profile_photo_path) : asset('img/profilePhotoDefault.png') }}"
                                     class="profile-photo" alt="Foto de Perfil">
                                <label for="profile_photo" class="edit-icon">
                                    <i class="fas fa-pencil-alt"></i>
                                </label>
                                <input type="file" id="profile_photo" name="profile_photo" accept="image/*">
                            </div>
                        </div>

                        <div class="mb-3">
                            <label for="name" class="form-label">Nome</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}" required>
                        </div>

                        <div class="mb-3">
                            <label for="email" class="form-label">Email</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}" required>
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Papel do Usuário</label>
                            @foreach($papels as $papel)
                                <div class="form-check">
                                    <input type="radio" class="form-check-input" id="papel_{{ $papel->id }}" name="papel_id" value="{{ $papel->id }}" 
                                           {{ $user->papel_id == $papel->id ? 'checked' : '' }} required>
                                    <label for="papel_{{ $papel->id }}" class="form-check-label">{{ $papel->nome }}</label>
                                </div>
                            @endforeach
                        </div>

                        <div class="d-flex justify-content-end mt-4">
                            <a href="{{ route('users.index') }}" class="btn btn-secondary me-2">Cancelar</a>
                            <button type="submit" class="btn btn-primary">Salvar Alterações</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <script>
            document.getElementById('profile_photo').addEventListener('change', function(event) {
                const reader = new FileReader();
                reader.onload = function(e) {
                    document.getElementById('profilePreview').src = e.target.result;
                };
                reader.readAsDataURL(event.target.files[0]);
            });
        </script>
    </x-app-layout>
@endsection
