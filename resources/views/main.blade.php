@extends('layouts.main')
@section('title', 'Inicial')
@section('content')
    <style>
        a:hover {
            color: #0d6efd;
        }

        @media (max-width: 768px) {

            .bg-bullet {
                display: none;
            }

            .navApp {
                display: none;
            }

            .container-fluid{

                flex-wrap: nowrap !important;

            }
        }
    </style>
    <main class="container-fluid flex-row  h-5">
        <div class="row h-100 ">
            <div class="col bg-bullet">
                <div class="row">
                    <div class="col d-flex justify-content-center align-items-center flex-column mt-5">
                        <div class="title mb-5">
                            <h1 class="text-center display-1 fw-semibold logo-text">Avante Notify</h1>
                            <p class="text-center mb-5" style="font-weight: 500; font-size: 1.2rem">A Plataforma de
                                Comunicação que Você Precisa! </p>
                        </div>
                        <img src="{{ asset('img/notifications.png') }}" width="65%" style="" alt="notificações"
                            class="mt-5">

                    </div>
                </div>
            </div>
            <div class="col m-0 p-0">
                <div class="container bg-white p-0">
                    @include('auth/login')
                </div>
            </div>
        </div>
    </main>

@endsection
