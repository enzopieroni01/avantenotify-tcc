@extends('layouts.main')
@section('title', 'Chat')
@section('styles')
    <style>
        .data {
            display: inline-block;
            font-size: 1%;
        }

        .container {
            display: flex;
            height: 100%;
            padding: 0;
        }

        .user-list {
            width: 25%;
            border-right: 1px solid #ddd;
            overflow-y: auto;
            background-color: #f8f9fa;
            padding: 10px;
            border-radius: 10px;
            box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
        }

        .chat-window {
            width: 75%;
            display: flex;
            flex-direction: column;
            padding: 10px;
        }

        .messages {
            flex: 1;
            padding: 15px;
            overflow-y: auto;
            border-bottom: 1px solid #ddd;
            max-height: 60vh;
            background-color: #f8f9fa;
            background-image: url('img/chatBg.png');
            /* Seu caminho da imagem de fundo */
            background-size: cover;
            /* Ajusta a imagem para cobrir todo o espaço */
            background-position: center;
            /* Centraliza a imagem horizontal e verticalmente */
            background-repeat: no-repeat;
            /* Não repete a imagem */
            min-height: 60vh;
            border-radius: 10px;
            box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
        }


        .messages::before {
            content: "";
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-image: url('img/chatBg.png');
            background-size: cover;
            background-position: center;
            background-repeat: repeat;
            opacity: 0.8;
            z-index: -1;
            border-radius: 10px;
        }


        .messages::-webkit-scrollbar {
            width: 8px;
        }

        .messages::-webkit-scrollbar-track {
            background: #f1f1f1;
            border-radius: 10px;
        }

        .messages::-webkit-scrollbar-thumb {
            background: #007bff49;
            border-radius: 10px;
        }

        .messages::-webkit-scrollbar-thumb:hover {
            background: #0057b3c3;
        }

        .message {
            display: block;
            margin-bottom: 10px;
            padding: 10px 15px;
            border-radius: 15px;
            min-width: 57px;
            max-width: 50%;
            word-wrap: break-word;
            width: max-content;
            box-shadow: 0 3px 6px rgba(0, 0, 0, 0.1);
        }

        .message.sent small {
            font-size: 65%;
            text-align: right;
            display: block;
        }

        .message.received small {
            font-size: 65%;
            text-align: left;
            display: block;
        }

        .sent {
            background-color: #007bff;
            color: white;
            align-self: flex-end;
            border-end-end-radius: 0;
        }

        .received {
            background-color: #d2d2d2;
            color: #333;
            align-self: flex-start;
            border-end-start-radius: 0;
        }

        .message.sent {
            margin-left: auto;
            margin-right: 10px;
        }

        .message.received {
            margin-right: auto;
            margin-left: 10px;
        }

        .messages-input {
            padding: 15px;
            display: flex;
            background-color: #fff;
            border-top: 1px solid #ddd;
            border-radius: 0 0 10px 10px;
        }

        .messages-input textarea {
            flex: 1;
            margin-right: 10px;
            border-radius: 10px;
            padding: 10px;
            box-shadow: inset 0 2px 5px rgba(0, 0, 0, 0.1);
            border: 1px solid #ddd;
        }

        .messages-input textarea:focus {
            border-color: #007bff;
            outline: none;
            box-shadow: inset 0 2px 5px rgba(0, 0, 0, 0.2);
        }

        #send {
            background-color: #007bff;
            width: 65px;
            border: none;
            padding: 10px;
            color: white;
            display: flex;
            justify-content: center;
            align-items: center;
            box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
            transition: background-color 0.2s;
        }

        #send:hover {
            background-color: #0056b3;
        }

        .user-list-item {
            cursor: pointer;
            padding: 10px 15px;
            border-bottom: 1px solid #ddd;
            display: flex;
            justify-content: flex-start;
            align-items: center;
            background-color: #fff;
            border-radius: 10px;
            margin-bottom: 5px;
            transition: background-color 0.2s, box-shadow 0.2s;
        }

        .user-list-item:hover {
            background-color: #f1f1f1;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        }

        .user-list-item.active {
            background-color: #007bff;
            color: white;
            box-shadow: 0 4px 10px rgba(0, 0, 0, 0.1);
        }

        .user-list-item.active .user-name {
            color: white;
        }

        .hiddenTitle {
            display: none;
        }

        .profile-photo {
            width: 40px;
            height: 40px;
            border-radius: 50%;
            object-fit: cover;
            display: inline;
            margin-right: 10px;
        }

        .user-name {
            font-size: 16px;
            font-weight: bold;
            text-align: left;
            max-width: 150px;
            display: inline-block;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }

        .cardChat {
            border-radius: 10px;
            box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
            background-color: #fff;
        }

        #tituloChat {
            padding: 10px;
            font-size: 20px;
            font-weight: bold;
            background-color: #f1f1f1;
            border-radius: 10px;
        }

        @media (max-width: 768px) {

            .dNone{
                display: none;
            }

            .container.chatPusher {
                flex-direction: column;
                flex: 1;
                /* Faz a div expandir para ocupar o espaço disponível */
            }

            .navApp {
                display: none;
            }

            .chat-window {
                width: 100%;
            }

            .user-list {
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100vh;
                background-color: #fff;
                transform: translateX(-100%);
                transition: transform 0.3s ease-in-out;
            }

            .user-list.active {
                transform: translateX(0);
            }

            .user-list-item {
                /* ... (estilos existentes) ... */
                cursor: pointer;
            }

            .messages {
                min-height: 67vh;
                display: grid;
                /* Define o contêiner como uma grade */
                place-items: center;
                /* Centraliza o conteúdo (imagem) vertical e horizontalmente */
                height: 100%;
            }

            img {
                max-width: 100%;
                /* Garante que a imagem não exceda a largura da div */
                height: auto;
            }

            .menu-toggle {
                background-color: #007BFF;
                color: white;
                border: none;
                border-radius: 30px;
                padding: 12px 30px;
                font-size: 18px;
                cursor: pointer;
                transition: all 0.3s ease;
                display: flex;
                align-items: center;
                box-shadow: 0 4px 15px rgba(0, 123, 255, 0.5);
            }


            /* Efeito de clique */
            .menu-toggle:active {
                transform: translateY(1px);
                /* Diminui ao clicar */
            }

            /* Adicionando um ícone ao botão */

        }
    </style>

@endsection
@section('content')
    <x-app-layout>
        <div class="d-flex justify-content-center align-items-center min-h-screen">

            <div class="container chatPusher">
                
                <nav class="user-list" id="user-list">
                    <div class="input-group mb-2">
                        <input type="search form-control" name="search" class="form-control" placeholder="Buscar Usuários...">
                        <span class="input-group-btn">
                            <button type="button" id="searchbtn" class="btn btn-primary"><i
                                    class="fa-solid fa-magnifying-glass"></i></button>
                        </span>
                    </div>
                    <ul class="list-group" id="user-list">
                        @foreach ($users as $user)
                            <li class="list-group-item list-group-item-action user-list-item" data-id="{{ $user->id }}"
                                data-nome="{{ $user->name }}">
                                @if (!$user->profile_photo_path)
                                    <img src="{{ asset('img/profilePhotoDefault.png') }}" class="profile-photo">
                                @else
                                    <img src="{{ asset('storage/' . $user->profile_photo_path) }}" class="profile-photo">
                                @endif
                                <span class="user-name">{{ $user->name }}</span>
                            </li>
                        @endforeach
                    </ul>
                </nav>
                <div class="chat-window" id="chat-window">
                    <div class="cardChat">
                        <div class="d-flex flex-row">
                            <button id="menu-toggle" class="menu-toggle me-auto">☰ Usuários</button>
                            <form method="POST" action="{{ route('logout') }}" x-data class="d-flex justify-content-center align-items-center">
                                @csrf
                                <x-dropdown-link href="{{ route('logout') }}" @click.prevent="$root.submit();">
                                    <span class="text-danger">Sair <i class="fa-solid fa-right-from-bracket"></i></span>
                                </x-dropdown-link>
                            </form>
                        </div>
                        <div id="chat-header" class="card-header">
                            
                            <b class="fs-4 titulo">Chat via Push <i class="fa-regular fa-comments"></i></b>
                        </div>
                        <div class="card-header hiddenTitle" id="tituloChat"></div>
                        <div class="messages card-body" id="messages">

                            <img src="{{ asset('img/chatBgMain1.png') }}">
                        </div>
                        <div class="messages-input card-footer">
                            <textarea id="message-input" class="form-control" placeholder="Mensagem"></textarea>
                            <button id="send" class="btn btn-primary">
                                <svg xmlns="http://www.w3.org/2000/svg" width="2em" height="2em" viewBox="0 0 24 24">
                                    <g fill="none">
                                        <path fill="currentColor"
                                            d="m7.24 4.535l11.944 5.658c1.525.722 1.525 2.892 0 3.614L7.24 19.466c-1.415.67-3.017-.472-2.844-2.028l.58-5.216a2 2 0 0 0 0-.442l-.58-5.216c-.173-1.557 1.429-2.7 2.844-2.029"
                                            opacity="0.16" />
                                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                                            stroke-width="2"
                                            d="m5 12l-.604-5.437C4.223 5.007 5.825 3.864 7.24 4.535l11.944 5.658c1.525.722 1.525 2.892 0 3.614L7.24 19.466c-1.415.67-3.017-.472-2.844-2.028zm0 0h7" />
                                    </g>
                                </svg>
                            </button>
                        </div>
                        <div id="response"></div>
                    </div>
                </div>
            </div>
        </div>
    </x-app-layout>
@endsection

@section('scripts')

    <script src="https://code.jquery.com/jquery-3.7.1.min.js"
        integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
    <script src="https://js.pusher.com/8.2.0/pusher.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>



    <script type="module">
        document.addEventListener("DOMContentLoaded", function() {

            Pusher.logToConsole = true;
            let selectedUserId = null;
            let selectedUserName = null;
            let userLogado = "{{ Auth::user()->id }}";
            let idChatStr = null;

            // Configuração do Pusher 
            var pusher = new Pusher("{{ env('PUSHER_APP_KEY') }}", {
                cluster: "{{ env('PUSHER_APP_CLUSTER') }}",
                encrypted: true,
                authEndpoint: '{{ route('chat.auth') }}',
                auth: {
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    }
                }
            });

            jQuery(function() {
                // CSRF automatico nas requisições ajax
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                let arr;
                $('#menu-toggle').click(function() {
                    $('#menu-toggle').addClass('dNone');
                    $('.user-list').addClass('active');
                    
                    $('.user-list-item').on('click', function() {
                        selectedUserName = $(this).data('nome');
                        selectedUserId = $(this).data('id');

                        $('.user-list').removeClass('active');
                        $('#menu-toggle').removeClass('dNone');
                        $('.titulo').addClass('dNone');

                        $('#tituloChat').removeClass('hiddenTitle');
                        $('#tituloChat').html('<b class="fs-5"> Chat com ' +
                            selectedUserName + '</b>');

                        $('.messages').html('');
                        $('.user-list-item').removeClass('active');
                        $(this).addClass('active');

                        arr = [selectedUserId, userLogado];
                        arr.sort();
                        idChatStr = arr.join("-");
                        console.log(idChatStr);

                        //ajax para buscar msgs antigas do chat
                        $.ajax({
                            url: '/chat/messages/' + selectedUserId,
                            method: 'GET',
                            success: function(mensagens) {
                                mensagens.forEach(function(mensagem) {
                                    let messageClass = mensagem
                                        .usuario_remetente_id ==
                                        userLogado ?
                                        'sent' : 'received';
                                    $('#messages').append(
                                        '<div class="message ' +
                                        messageClass +
                                        '"><span>' + mensagem
                                        .mensagem +
                                        '</span> <br><small class="text-muted">' +
                                        moment(mensagem.dataHora)
                                        .format(
                                            'dddd, HH:mm') +
                                        '</small></div>'
                                    );
                                });
                                scrollToBottom();
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                console.error(
                                    'Erro ao buscar mensagens antigas:',
                                    textStatus);
                            }
                        });

                        // se inscreve no canal privado de acordo com o id do usario logado  e o escolhido com id ordenados para nao dar conflito
                        const channel = pusher.subscribe('private-my-channel-' + idChatStr);
                        channel.bind('my-event', function(data) {
                            let messageClass = data.user_id == userLogado ? 'sent' :
                                'received';

                            // mostra msg enviada
                            $('#messages').append(
                                '<div class="message ' + messageClass +
                                '"><span>' + data
                                .message +
                                '</span> <br><small class="text-muted">' +
                                moment().format(
                                    'dddd, HH:mm') + ' </small></div>'
                            );
                        });
                    });
                });
                // envia msg
                $(document).on('click', '#send', function() {
                    let message = $('#message-input').val();

                    // verifica se o usuario foi selecionado
                    if (!selectedUserId) {
                        Swal.fire({
                            title: "Ops!",
                            text: "Parece que você esqueceu de escolher para quem você quer enviar uma mensagem 😅",
                            icon: "warning",
                            showConfirmButton: true,
                            focusConfirm: false,
                            confirmButtonText: "Escolher contato"
                        });
                        return;
                    }

                    // verifica se a mensagem não ta vazia
                    if (message.trim()) {
                        //AJAX para enviar a mensagem
                        $.ajax({
                            url: '{{ route('chat.send') }}',
                            type: 'POST',
                            data: {
                                'message': message,
                                'idChat': idChatStr,
                                'usuario_destinatario_id': selectedUserId
                            },
                            success: function(resposta) {
                                if (resposta.status == 'sucesso') {
                                    console.log(resposta.mensagem);
                                } else {
                                    $('#response').html(
                                        '<p style="color:red;">' + resposta
                                        .mensagem + '</p>'
                                    );
                                }
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                $('#response').html(
                                    '<p style="color:red;">Erro ao enviar mensagem: ' +
                                    textStatus + '</p>'
                                );
                            }
                        });

                        $('#message-input').val("");
                    } else {
                        Swal.fire({
                            title: "Cadê a Mensagem?",
                            text: "Parece que você se esqueceu de escrever uma mensagem 😅​",
                            icon: "question"
                        });
                    }
                });

            });
        });

        function scrollToBottom() {
            var messages = document.getElementById('messages');
            messages.scrollTop = messages.scrollHeight;
        }
    </script>
@endsection
