<?php

use App\Events\MessageSent;
use App\Http\Controllers\PapeisController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\MainController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\MailController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\NotificacaoController;
use App\Models\Notificacao;
use App\Models\User;
use App\Services\MailtrapService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Laravel\Jetstream\Http\Controllers\Inertia\UserProfileController;

Route::get('/', [MainController::class, 'main'])->name('main');


Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', [DashboardController::class, 'main'])->name('dashboard');

    /* Rotas do Chat Pusher */
    Route::get('/chat', [NotificacaoController::class, 'index'])->name('chat');
    Route::post('/chat-autenticar-users', [NotificacaoController::class, 'autenticar'])->name('chat.auth');
    Route::post('/chat/send-message', [NotificacaoController::class, 'sendMessage'])->name("chat.send");
    Route::get('/chat/messages/{usuario_destinatario_id}', [NotificacaoController::class, 'fetchMessages']);
    Route::post('/notificacao/ler/{usuarioQueEstaConversando}', [NotificacaoController::class, 'marcarNotificacoesComoLidas']);

    // Rotas para o chat mobile
    Route::get('/chat-mobile', [NotificacaoController::class, 'mobileChat'])->name('chat.mobile');
    Route::post('/chat-mobile/autenticar-users', [NotificacaoController::class, 'autenticar'])->name('chat.mobile.auth');
    Route::post('/chat-mobile/send-message', [NotificacaoController::class, 'sendMessage'])->name("chat.mobile.send");
    Route::get('/chat-mobile/messages/{usuario_destinatario_id}', [NotificacaoController::class, 'fetchMessages']);


    /* Rotas do Email */
    Route::get('/email', [MailController::class, 'main'])->name('email');
    Route::post('/email/send-email', [MailController::class, 'index'])->name('emailSend');
    Route::post('/upload-image', [MailController::class, 'uploadImage'])->name('uploadImage');
    Route::get('/inbox', [MailController::class, 'listEmails'])->name('inbox');
    Route::get('/inbox/{inboxId}/{messageId}', [MailController::class, 'showEmail'])->name('showEmail');
});

/* Rotas que so Admin pode acessar */
Route::middleware(['admin'])->group(function () {
    Route::resource('users', UsersController::class);
    Route::get('/usuarios/list', [UsersController::class, 'list'])->name('users.list');
    Route::resource('papeis', PapeisController::class);
    // Route::get('/search', [UsersController::class, 'search'])->name('users.search');
});
