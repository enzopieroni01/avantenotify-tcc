<?php

use Illuminate\Support\Facades\Broadcast;
use Illuminate\Support\Facades\Log;

Broadcast::channel('chat.{receiverId}', function ($user, $receiverId) {
    // Log::info('Deu certo', ['user' => $user->id, 'receiver' => $receiverId]);
    // return (int) $user->id === (int) $receiverId;
    return true;
    // Log::info('Tentativa de acessar o chat', ['user_id' => $user->id, 'receiver_id' => $receiverId]);
    // return (int) $user->id === (int) $receiverId || (int) $user->id === (int) request()->sender_id;
});

Broadcast::routes(['middleware' => ['api', 'auth:sanctum']]);
